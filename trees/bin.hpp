#ifndef TREES_BINARY_TREE_HPP
#define TREES_BINARY_TREE_HPP

#include <memory>

///
/// \brief Binary Tree Node
///
template<typename T>
class BinaryNode {
public:
    using value_type = T;
    using value_pointer = value_type *;
    using value_reference = value_type &;
    using value_const_reference = const value_type &;

    using node_type = BinaryNode<value_type>;
    using node_pointer = node_type *;
    using node_const_pointer = const node_type *;

    static std::unique_ptr<BinaryNode<T>> create(value_type &&val);

    bool is_leaf() const noexcept;

    node_const_pointer find(value_const_reference val) const;
    node_const_pointer insert(value_type &&val);

    value_const_reference value() const;
    node_const_pointer left() const;
    node_const_pointer right() const;

private:
    explicit BinaryNode(value_type &&value) noexcept
        : _value{std::move(value)} {
    }

    value_type _value;
    std::unique_ptr<node_type> _left;
    std::unique_ptr<node_type> _right;
};

/*
 * inline implementation
 */

template<typename T>
std::unique_ptr<BinaryNode<T>> BinaryNode<T>::create(value_type &&val) {
    class MakeUniqueEnabler : public BinaryNode<T> {
    public:
        explicit MakeUniqueEnabler(value_type &&val)
            : BinaryNode<T>(std::move(val)) {
        }
    };
    return std::make_unique<MakeUniqueEnabler>(std::move(val));
}

template<typename T>
bool BinaryNode<T>::is_leaf() const noexcept {
    return _left == nullptr && _right == nullptr;
}

template<typename T>
typename BinaryNode<T>::node_const_pointer BinaryNode<T>::find(value_const_reference val) const {
    auto node = this;

    while (node) {
        if (node->value() == val) {
            return node;
        } else if (val < node->value()) {
            node = node->left();
        } else {
            node = node->right();
        }
    }

    return nullptr;
}

template<typename T>
typename BinaryNode<T>::node_const_pointer BinaryNode<T>::insert(value_type &&val) {
    node_pointer node = this;
    while (node) {
        if (node->value() == val) {
            return node;
        } else if (val < node->value()) {
            if (node->left()) {
                node = node->_left.get();
            } else {
                node->_left = create(std::move(val));
                return node->_left.get();
            }
        } else {
            if (node->right()) {
                node = node->_right.get();
            } else {
                node->_right = create(std::move(val));
                return node->_right.get();
            }
        }
    }

    return node;
}

template<typename T>
typename BinaryNode<T>::value_const_reference BinaryNode<T>::value() const {
    return _value;
}

template<typename T>
typename BinaryNode<T>::node_const_pointer BinaryNode<T>::left() const {
    return _left.get();
}

template<typename T>
typename BinaryNode<T>::node_const_pointer BinaryNode<T>::right() const {
    return _right.get();
}

#endif//TREES_BINARY_TREE_HPP
