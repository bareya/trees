#ifndef TREES_TESTS_BINARY_TREE_CPP_H
#define TREES_TESTS_BINARY_TREE_CPP_H

#include <doctest.h>
#include <trees/bin.hpp>

TEST_SUITE("binary tree tests") {

    TEST_CASE("is first node a leaf") {
        auto binary_node = BinaryNode<int>::create(0);
        REQUIRE(binary_node->is_leaf() == true);
    }

    TEST_CASE("append left child") {
        auto binary_node = BinaryNode<int>::create(0);
        binary_node->insert(-1);

        REQUIRE(binary_node->is_leaf() == false);
        REQUIRE(binary_node->left() != nullptr);
        REQUIRE(binary_node->right() == nullptr);
    }

    TEST_CASE("append right child") {
        auto binary_node = BinaryNode<int>::create(0);
        binary_node->insert(1);

        REQUIRE(binary_node->is_leaf() == false);
        REQUIRE(binary_node->left() == nullptr);
        REQUIRE(binary_node->right() != nullptr);
    }

    TEST_CASE("insert element with the same value") {
        auto binary_node = BinaryNode<int>::create(0);
        REQUIRE(binary_node->is_leaf() == true);

        binary_node->insert(0);
        REQUIRE(binary_node->is_leaf() == true);
    }

    TEST_CASE("append left and right child") {
        auto binary_node = BinaryNode<int>::create(0);
        binary_node->insert(-1);
        binary_node->insert(1);

        REQUIRE(binary_node->left() != nullptr);
        REQUIRE(binary_node->right() != nullptr);
    }

    TEST_CASE("append left elements") {
        auto binary_node = BinaryNode<int>::create(0);
        binary_node->insert(-1);
        binary_node->insert(-2);

        REQUIRE(binary_node->value() == 0);

        SUBCASE("") {
            REQUIRE(binary_node->left() != nullptr);
            REQUIRE(binary_node->right() == nullptr);
        }


        REQUIRE(binary_node->left()->value() == -1);

        REQUIRE(binary_node->left()->left() != nullptr);
        REQUIRE(binary_node->left()->left()->value() == -2);

        REQUIRE(binary_node->left()->left()->left() == nullptr);
    }

    TEST_CASE("append right elements") {
        auto binary_node = BinaryNode<int>::create(0);
        binary_node->insert(1);
        binary_node->insert(2);
    }
}

#endif//TREES_TESTS_BINARY_TREE_CPP_H
